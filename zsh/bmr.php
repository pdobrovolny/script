#!/usr/bin/env php
<?php

declare(strict_types=1);

$dateTime = new DateTime();

echo 'servisní heslo: '.decbin(((int)$dateTime->format('Y') - 1900) % 16) . ' ' . decbin((int)$dateTime->format('m')) . "\n";
