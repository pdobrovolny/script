#!/usr/bin/env sh

export TERM=xterm
set -e

echo Adding submodules
git config -f .gitmodules --get-regexp '^submodule\..*\.path$' |
  while read -r path_key path; do
    url_key=$(echo "$path_key" | sed 's/\.path/.url/')
    url=$(git config -f .gitmodules --get "$url_key")
    git submodule add -f "$url" "$path"
  done

echo Syncing submodules
git submodule sync --recursive >/dev/null

echo Checkout submodules
SCRIPTDIR=$(dirname "$0")
"$SCRIPTDIR"/checkoutSubmodules.sh "${CI_COMMIT_BRANCH}"
