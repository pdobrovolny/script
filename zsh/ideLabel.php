#!/usr/bin/env php
<?php
declare(strict_types=1);

namespace pdobrovolny;

use Jawira\CaseConverter\Convert;

include $_composer_autoload_path ?? __DIR__ . '/../vendor/autoload.php';

\file_exists('./composer.json') === true || die;

$decode = \json_decode(\file_get_contents('./composer.json'), true, 512, \JSON_THROW_ON_ERROR);

$psr = $decode['autoload']['psr-4'] ?? null;

$explode = $psr === null
    ? \explode('/', $decode['name'] ?? '')
    : \explode('\\', \rtrim(\array_search('src/', $psr, true) ?: '', '\\'));

$group = \count($explode) === 1
    ? null
    : \array_shift($explode);

$convert = new Convert(\implode('', \array_map(\ucfirst(...), $explode)));
$project = \preg_replace('/\s?([A-Z])\s/', '$1', $convert->fromPascal()->toTitle());

$name = $group === null
    ? $project
    : \sprintf('%s (%s)', $project, $group);

echo $name . "\n";
\file_put_contents('./.idea/.name', $name);
