#!/usr/bin/env zsh

export TERM=xterm-256color
export RESET=$(tput setaf 246)
export WRN=$(tput setaf 1; tput bold)
export OK=$(tput setaf 2; tput bold)
export SUBMODULE=$(tput setaf 4)

function handleSubmodule() {
  if [ -z $(git diff --quiet || echo 'dirty') ]; then
    git fetch --all -p --prune > /dev/null;

    BRANCH=$(git show-ref | grep origin/"${REQUESTED}" | cut -d ' ' -f2 | sed -e "s#refs/remotes/origin/##")
    if [ "${BRANCH}" = "" ]; then BRANCH=${SOURCE_BRANCH}; fi

    COLOR=$(if [ "${BRANCH}" = "${SOURCE_BRANCH}" ]; then echo "${OK}"; else echo "${WRN}"; fi)

    echo "Checking branch ${COLOR}${BRANCH}${RESET} for submodule ${SUBMODULE}${1}${RESET}.";

    output=$(git checkout "${BRANCH}" 2>&1);
    checkoutCode=${?}
    if [ ${checkoutCode} -ne 0 ]; then echo "${WRN}${output}${RESET}"; else git reset --hard origin/"${BRANCH}"; fi
  else
    echo "${WRN}Cannot checkout submodule ${SUBMODULE}${1}${WRN}. Working tree is dirty!!! Commit and >>PUSH<< Your work!${RESET}";
  fi

  echo -------------------------;
}

export REQUESTED=$(if [ -z "${1}" ]; then git branch --show-current; else echo "${1}"; fi)

if [[ "${REQUESTED}" =~ "^release/*" ]]; then
  export REQUESTED="release/"
fi

export SOURCE_BRANCH=$(
if [ -z "$2" ];
  then case ${REQUESTED} in
    master | release/* | hotfix/*) echo "master";;
    *) echo "develop";;
  esac
  else echo "${2}";
fi
)

echo "${RESET}Checking out to ${OK}${REQUESTED}${RESET} branch with fallback ${OK}${SOURCE_BRANCH}${RESET} branch";
echo -------------------------;

git add "./submodule/*" 2> /dev/null

orig=$(pwd)
while read -r module
do
  cd "${module}" || exit 1
  handleSubmodule "${module}"
  cd "${orig}" || exit 1
done < <(grep path <./.gitmodules | cut -b 8-)

git submodule status | cut -b 43-;

case $(git config remote.origin.url) in
  *:apps/*\.git) git add "./submodule/*" > /dev/null;;
  *) git rm --cached -r -f "./submodule/*" > /dev/null;;
esac
