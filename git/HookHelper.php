<?php

declare( strict_types=1 );

namespace pdobrovolny;

final readonly class HookHelper {

	public const  C_VERSION = 'version';
	public const  C_NAME    = 'name';
	private const TMP       = '/tmp/';

	public static function executeGit( string $arguments ): string {
		return \trim( \shell_exec( sprintf( "git %s", $arguments ) ) ?? '' );
	}

	public static function getComposerComponent( string $composerFile, string $key ): ?string {
		$contents = file_get_contents( $composerFile );
		$decode = \json_decode( $contents, true, 512, JSON_THROW_ON_ERROR );

		return $decode[ $key ] ?? null;
	}

	public static function gitTagCleanup( int $historyVersions ): void {
		$allTagVersions = self::getAllTagVersions();
		$tagsDelete = \array_diff( $allTagVersions, \array_splice( $allTagVersions, -$historyVersions ) );

		foreach( $tagsDelete as $item ) {
			self::executeGit( "push -d origin $item" );
			self::executeGit( "tag -d $item" );
		}
	}

	public static function isBranchPrefix( string ...$prefixes ): bool {
		$localBranch = self::getBranch();

		return \count( \array_filter( $prefixes, static fn( string $prefix ): bool => str_starts_with( $localBranch, $prefix ) ) ) > 0;
	}

	public static function lock( string $lockFileName, string $projectName ): bool {
		$lockFile = self::makeLockFile( $lockFileName );
		$locks = self::getLocks( $lockFile );

		if( \in_array( $projectName, $locks ) ) {
			return false;
		}

		$locks[] = $projectName;
		self::saveLocks( $lockFile, $locks );

		return true;
	}

	public static function setTag( string $tag ): void {
		self::executeGit( "tag -a $tag -m \"\"" );
		self::executeGit( "push --tags" );
	}

	public static function unlock( string $lockFileName, string $projectName ): void {
		$lockFile = self::makeLockFile( $lockFileName );
		self::saveLocks(
			$lockFile,
			array_filter(
				self::getLocks( $lockFile ),
				static fn( string $name ): bool => $name !== $projectName )
		);
	}

	public static function updateComposerVersion( string $composerFile, string ...$prefixesIncr ): string {
		$isIncr = self::isBranchPrefix( ...$prefixesIncr );

		$contents = file_get_contents( $composerFile );
		$decode = \json_decode( $contents, true, 512, JSON_THROW_ON_ERROR );

		$currentVersion = $decode[ self::C_VERSION ] ?? die;
		$newVersion = self::makeNewVersion( $currentVersion, $isIncr );

		\file_put_contents(
			$composerFile,
			\preg_replace( "/(.*\"version\":\\s*)(\"$currentVersion\")(.*)/", "$1\"$newVersion\"$3", $contents ),
		);

		return $newVersion;
	}

	private static function getAllTagVersions(): array {
		$filter = \array_filter(
			\array_unique( \explode( "\n", self::executeGit( "--no-pager tag" ) ) ),
			static fn( string $ver ): bool => \preg_match( '/\d+(\.\d+)+/', $ver ) === 1
		);

		\usort( $filter, static fn( string $v1, string $v2 ): int => \version_compare( $v1, $v2 ) );

		return $filter;
	}

	private static function getBranch(): string {
		return self::executeGit( "rev-parse --abbrev-ref HEAD" );
	}

	private static function getGitCommitsCount(): int {
		return (int) self::executeGit( "rev-list --count origin/HEAD" );
	}

	private static function getGitLatestTag(): ?string {
		$versions = self::getAllTagVersions();

		return \end( $versions ) ?: null;
	}

	/**
	 * @return list<string>
	 */
	private static function getLocks( string $lockFile ): array {
		\touch( $lockFile );

		return \json_decode( file_get_contents( $lockFile ) ?: '[]', true, 512, JSON_THROW_ON_ERROR );
	}

	private static function makeLockFile( string $lockFileName ): string {
		return self::TMP . $lockFileName;
	}

	private static function makeNewVersion( string $currentVersion, bool $isIncr ): string {
		[ $maj, $min ] = self::versionAsArray( $currentVersion );

		if( $isIncr ) {
			$latestVer = self::getGitLatestTag();
			$min = $latestVer === null ? $min : self::versionAsArray( $latestVer )[ 1 ] + 1;
		}

		return \implode( '.', [ $maj, $min, self::getGitCommitsCount() ] );
	}

	/**
	 * @param array<string,bool> $locks
	 */
	private static function saveLocks( string $lockFile, array $locks ): void {
		\file_put_contents( $lockFile, \json_encode( $locks, JSON_THROW_ON_ERROR ) );
	}

	/**
	 * @param string $version
	 *
	 * @return array{0: string, 1: string}
	 */
	private static function versionAsArray( string $version ): array {
		$explode = \explode( '.', $version );
		$maj = $explode[ 0 ] ?? '0';
		$min = $explode[ 1 ] ?? '0';

		return [ $maj, $min ];
	}
}
