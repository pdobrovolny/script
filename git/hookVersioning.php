#!/usr/local/bin/php
<?php

namespace pdobrovolny;

require_once __DIR__ . '/HookHelper.php';

// Pozn. k instalaci:
// hook použít pro: post-commit, post-rewrite, post-merge

$fileName = 'composer.json';
$lockFile = '.gitCommit';
$gitDir = ( $_SERVER[ 'PWD' ] ?? __DIR__ ) . \DIRECTORY_SEPARATOR;
$composerFile = $gitDir . $fileName;

\file_exists( $composerFile ) === true || die;
$projectName = HookHelper::getComposerComponent( $composerFile, HookHelper::C_NAME ) ?? die;
$currentVersion = HookHelper::getComposerComponent( $composerFile, HookHelper::C_VERSION ) ?? die;

//composer
if( HookHelper::isBranchPrefix( 'hotfix/', 'release/', 'develop', 'feature/', 'bugfix/' ) ) {
	if( \file_exists( $gitDir . '.git' . DIRECTORY_SEPARATOR . 'MERGE_HEAD' ) === true ) {
		echo "Merge in progress, skipping versioning";
		die;
	}

	HookHelper::lock( $lockFile, $projectName ) === true || die;

	echo \sprintf( 'Updating composer.json to new version `%s`', HookHelper::updateComposerVersion( $composerFile, 'develop', 'feature/', 'bugfix/' ) ) . PHP_EOL;

	HookHelper::executeGit( 'add ' . $fileName );
	HookHelper::executeGit( 'commit --amend -C HEAD --no-verify' );

	HookHelper::unlock( $lockFile, $projectName );
}

// tags
if( HookHelper::isBranchPrefix( 'master', 'main' ) ) {
	HookHelper::lock( $lockFile, $projectName ) || die;

	echo \sprintf( 'Adding tag `%s`', $currentVersion ) . PHP_EOL;
	HookHelper::setTag( $currentVersion );

	HookHelper::gitTagCleanup( 10 );

	HookHelper::unlock( $lockFile, $projectName );
}
